#  Copyright (c) 2021. Michael Kemna.
from datetime import date, datetime, timedelta

import plac

from app.AvailabilityBootstrapper import AvailabilityBootstrapper
from app.forecastBootstrapper import ForecastBootstrapper
from app.resortBootstrapper import ResortBootstrapper
from app.weatherBootstrapper import WeatherBootstrapper


@plac.opt("resort_id", "the resort id that you want to bootstrap", type=int)
@plac.opt("date_bootstrap", "the date from which you want to bootstrap")
def main(
    resort_id=-1,
    date_bootstrap=date.today(),
):
    """
    Bootstrap the database with dynamic data, wherein the dates are set to today.
    """
    print(f"resort_id = {resort_id}, date_bootstrap = {date_bootstrap}")
    # TODO: make the date injection actually work
    dt = datetime.combine(date_bootstrap, datetime.now().time())
    print(f"dt = {dt}")
    ResortBootstrapper().bootstrap(resort_id=resort_id)
    WeatherBootstrapper().bootstrap(resort_id=resort_id)
    [
        ForecastBootstrapper().bootstrap(
            resort_id=resort_id, date=date_bootstrap - timedelta(days=x)
        )
        for x in reversed(range(7))
    ]

    AvailabilityBootstrapper().bootstrap(resort_id=resort_id)
    print("Bootstrap successful")


if __name__ == "__main__":
    plac.call(main)
