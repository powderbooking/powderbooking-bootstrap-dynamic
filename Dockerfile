ARG ALPINE_VERSION="3.20"
# ref: https://hub.docker.com/_/python
ARG PYTHON_VERSION="3.13.0"
# ref: https://github.com/python-poetry/poetry/releases
ARG POETRY_VERSION="1.8.4"

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS base
ARG POETRY_VERSION
# ref: https://github.com/ufoscout/docker-compose-wait
ARG WAIT_VERSION=2.12.1

# https://gist.github.com/usr-ein/c42d98abca3cb4632ab0c2c6aff8c88a
# https://python-poetry.org/docs/configuration/#virtualenvsin-project
ENV \
  # python:
  # dump the traceback on error
  PYTHONFAULTHANDLER=1 \
  # write messages directly to stdout instead of buffering
  PYTHONUNBUFFERED=1 \
  # prevents python creating .pyc files
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=1 \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_HOME="/usr/local/poetry" \
  POETRY_VIRTUALENVS_IN_PROJECT=1 \
  # do not ask any interactive question
  POETRY_NO_INTERACTION=1 \
  POETRY_CACHE_DIR='/var/cache/pypoetry' \
  # # paths where everything gets installed based on POETRY_VIRTUALENVS_IN_PROJECT
  VENV_PATH="/.venv"

ENV PATH="$VENV_PATH/bin:$PATH"

# install runtime dependencies
# https://github.com/ufoscout/docker-compose-wait
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/${WAIT_VERSION}/wait /wait
RUN chmod +x /wait

FROM base AS build

ENV PATH="$POETRY_HOME/bin:$PATH"

RUN apk add --update curl && rm -rf /var/cache/apk/*
RUN curl -sSL https://install.python-poetry.org | python3 - --version ${POETRY_VERSION}

RUN --mount=type=bind,source=poetry.lock,target=poetry.lock \
  --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
  poetry install --no-root --no-dev --no-ansi

FROM base AS prod

COPY --from=build $VENV_PATH $VENV_PATH
COPY . .

# Create a non-privileged user that the app will run under.
# See https://docs.docker.com/go/dockerfile-user-best-practices/
ARG UID=10001
RUN adduser \
  --disabled-password \
  --gecos "" \
  --home "/nonexistent" \
  --shell "/sbin/nologin" \
  --no-create-home \
  --uid "${UID}" \
  appuser

USER appuser

ENTRYPOINT ["./entrypoint.sh"]