#  Copyright (c) 2021. Michael Kemna.

from app.forecast import forecast


def test_forecasts():
    # arrange
    # act
    result = forecast(1)
    # assert
    assert len(result["forecast_days"]) == 7
