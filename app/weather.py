#  Copyright (c) 2021. Michael Kemna.

from datetime import datetime

from app.api.weatherApi import WeatherPostDto
from app.config import DATETIME_BACKEND_FORMAT


def weathers(resort_id: int, dt: datetime = datetime.now()) -> WeatherPostDto:
    weather_lookup = {
        -1: {
            "resort_id": -1,
            "dt": dt.strftime(DATETIME_BACKEND_FORMAT),
            "temperature_c": 0.21,
            "wind_speed_kmh": 2.42,
            "wind_direction_deg": 210,
            "visibility_km": 10000,
            "clouds_pct": 90,
            "snow_3h_mm": 2,
            "rain_3h_mm": 2,
        }
    }

    return weather_lookup[resort_id]
