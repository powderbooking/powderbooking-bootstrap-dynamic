#  Copyright (c) 2021. Michael Kemna.

from datetime import date, timedelta

from app.api.forecastApi import ForecastDayPostDto, ForecastPostDto


def forecast(resort_id, forecast_date=date.today()) -> ForecastPostDto:
    return {
        "resort_id": resort_id,
        "date": str(forecast_date),
        "rain_total_mm": 14,
        "snow_total_mm": 7,
        "forecast_days": [
            forecastDay(resort_id, forecast_date + timedelta(days=i))
            for i in range(0, 7)
        ],
    }


def forecastDay(resort_id, forecast_date=date.today()) -> ForecastDayPostDto:
    return {
        "resort_id": resort_id,
        "date": str(forecast_date),
        "temperature_max_c": 11,
        "temperature_min_c": 10,
        "rain_total_mm": 2,
        "snow_total_mm": 1,
        "prob_precip_pct": 30,
        "wind_speed_max_kmh": 2,
        "windgst_max_kmh": 5,
    }
