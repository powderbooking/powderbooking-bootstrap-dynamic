import json
from datetime import date, datetime
from typing import List, TypedDict

from requests import get, post

from app.config import BACKEND_API


class AvailabilityPostDto(TypedDict):
    resort_id: int
    name: str
    url: str
    rating: str
    checkin: date
    checkout: date
    price: int


class AvailabilityDto(AvailabilityPostDto):
    id: int
    created: datetime


class BookingTargetsDto(TypedDict):
    id: int
    booking_dest_id: str


class BookingScrapeDto(TypedDict):
    targets: List[BookingTargetsDto]
    checkin: date
    checkout: date
    adults: int
    children: int
    rooms: int


def getAvailabilities(resortId: int) -> List[AvailabilityDto]:
    response = get(
        url=f"{BACKEND_API}/availability/{resortId}",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def getAvailabilityScrape() -> List[BookingScrapeDto]:
    response = get(
        url=f"{BACKEND_API}/availability/scrape",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def postAvailability(dto: AvailabilityPostDto) -> bool:
    response = post(
        url=f"{BACKEND_API}/availability",
        data=json.dumps(dto),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200


def postAvailabilities(dtos: List[AvailabilityPostDto]) -> bool:
    response = post(
        url=f"{BACKEND_API}/availability/bulk",
        data=json.dumps(dtos),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200
