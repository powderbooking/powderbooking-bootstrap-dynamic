import json
from datetime import datetime
from typing import List, Optional, TypedDict

from requests import get, post

from app.api.resortApi import ScrapeDto
from app.config import BACKEND_API


class WeatherPostDto(TypedDict):
    resort_id: int
    dt: datetime
    temperature_c: float
    wind_speed_kmh: float
    wind_direction_deg: float
    visibility_km: float
    clouds_pct: float
    snow_3h_mm: float
    rain_3h_mm: float


class WeatherDto(WeatherPostDto):
    id: int
    timepoint: int
    date: datetime
    created: datetime


def getWeather(resortId: int) -> Optional[WeatherDto]:
    response = get(
        url=f"{BACKEND_API}/weather/{resortId}",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def getWeatherScrape() -> List[ScrapeDto]:
    response = get(
        url=f"{BACKEND_API}/weather/scrape",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def postWeather(dto: WeatherPostDto) -> bool:
    response = post(
        url=f"{BACKEND_API}/weather",
        data=json.dumps(dto),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200


def postWeathers(dtos: List[WeatherPostDto]) -> bool:
    response = post(
        url=f"{BACKEND_API}/weather/bulk",
        data=json.dumps(dtos),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200
