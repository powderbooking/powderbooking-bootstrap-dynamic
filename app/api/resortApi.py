import json
from typing import Optional, TypedDict

import requests

from app.config import BACKEND_API


class ScrapeDto(TypedDict):
    id: int
    lat: float
    lng: float


class ResortDto(TypedDict):
    id: int
    booking_dest_id: str
    continent: str
    country: str
    village: str
    lat: float
    lng: float
    altitude_min_m: int
    altitude_max_m: int
    lifts: int
    slopes_total_km: int
    slopes_blue_km: int
    slopes_red_km: int
    slopes_black_km: int


def getResort(id: int) -> Optional[ResortDto]:
    response = requests.get(
        url=f"{BACKEND_API}/resort/{id}",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def postResort(id: int, dto: ResortDto) -> bool:
    response = requests.post(
        url=f"{BACKEND_API}/resort/{id}",
        data=json.dumps(dto),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200
