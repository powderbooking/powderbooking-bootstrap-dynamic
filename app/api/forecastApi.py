import json
from datetime import date, datetime
from typing import List, Optional, TypedDict

from requests import get, post

from app.api.resortApi import ScrapeDto
from app.config import BACKEND_API


class ForecastWeekPostDto(TypedDict):
    resort_id: int
    rain_total_mm: float
    snow_total_mm: float
    date: date  # still needs parsing though


class ForecastDayPostDto(TypedDict):
    resort_id: int
    date: date  # still needs parsing though
    temperature_max_c: float
    temperature_min_c: float
    rain_total_mm: float
    snow_total_mm: float
    prob_precip_pct: float
    wind_speed_max_kmh: float
    windgst_max_kmh: float


class ForecastDayDto(ForecastDayPostDto):
    id: int
    timepoint: int
    created: datetime


class ForecastDto(ForecastWeekPostDto):
    forecast_days: List[ForecastDayDto]


class ForecastPostDto(ForecastWeekPostDto):
    forecast_days: List[ForecastDayPostDto]


def getForecast(resortId: int, date: date) -> Optional[ForecastDto]:
    response = get(
        url=f"{BACKEND_API}/forecast/{resortId}/{date}",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def getForecastScrape() -> List[ScrapeDto]:
    response = get(
        url=f"{BACKEND_API}/forecast/scrape",
        headers={"Accept": "application/json"},
    )
    if response.status_code != 200:
        return
    return response.json()


def postForecast(dto: ForecastPostDto) -> bool:
    response = post(
        url=f"{BACKEND_API}/forecast",
        data=json.dumps(dto),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200


def postForecasts(dtos: List[ForecastPostDto]) -> bool:
    response = post(
        url=f"{BACKEND_API}/forecast/bulk",
        data=json.dumps(dtos),
        headers={"Content-Type": "application/json"},
    )
    return response.status_code == 200
