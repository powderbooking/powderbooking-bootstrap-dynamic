from datetime import date

from app.api.forecastApi import getForecast, postForecast
from app.bootstrapper import Bootstrapper
from app.config import date_parse
from app.forecast import forecast


class ForecastBootstrapper(Bootstrapper):
    def _check(self, resort_id: int, date: date) -> bool:
        knownForecast = getForecast(resort_id, date)

        return (
            knownForecast is not None
            and "date" in knownForecast
            and date_parse(knownForecast["date"]) == date
        )

    def _post(self, resort_id: int, date: date) -> bool:
        resorts = forecast(resort_id, date)
        return postForecast(resorts)
