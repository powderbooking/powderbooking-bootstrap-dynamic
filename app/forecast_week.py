#  Copyright (c) 2021. Michael Kemna.

from datetime import date, datetime


def forecast_week(resort_id: int, forecast_date: date = date.today()) -> dict:
    forecast_week_lookup = {
        -1: {
            "resort_id": -1,
            "date_request": str(datetime.now()),
            "date": str(forecast_date),
            "rain_week_mm": 14,
            "snow_week_mm": 7,
        }
    }

    return forecast_week_lookup[resort_id]
