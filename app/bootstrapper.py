from abc import ABC, abstractmethod

import requests
from circuitbreaker import circuit

from app.config import FAILURE_THRESHOLD


class Bootstrapper(ABC):
    def bootstrap(self, **kwargs) -> None:
        print(f"{type(self).__name__} Bootstrapping, kwargs {kwargs}")
        if not self._check_resilient(**kwargs):
            print(f"{type(self).__name__} NOT present")
            self._post_resilient(**kwargs)
            return

        print(f"{type(self).__name__} already present, skipping")

    @circuit(
        failure_threshold=FAILURE_THRESHOLD,
        expected_exception=requests.RequestException,
    )
    def _check_resilient(self, **kwargs) -> bool:
        return self._check(**kwargs)

    @abstractmethod
    def _check(self, **kwargs) -> bool:
        """
        Return true if properly bootstrapped, false if not
        """
        pass

    @circuit(
        failure_threshold=FAILURE_THRESHOLD,
        expected_exception=requests.RequestException,
    )
    def _post_resilient(self, **kwargs) -> None:
        if self._post(**kwargs):
            print(f"{type(self).__name__} posted")
        else:
            print(f"{type(self).__name__} FAILED posting")

    @abstractmethod
    def _post(self, **kwargs) -> bool:
        """
        Post the appropriate data to bootstrap
        """
        pass
