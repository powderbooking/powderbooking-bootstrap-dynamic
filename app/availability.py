#  Copyright (c) 2021. Michael Kemna.

from datetime import datetime, timedelta

from app.api.availabilityApi import AvailabilityPostDto


def availabilities(
    resort_id: int, checkin_date=datetime.now().date() + timedelta(days=2)
) -> AvailabilityPostDto:
    availability_lookup = {
        -1: {
            "resort_id": -1,
            "checkin": str(checkin_date),
            "checkout": str(checkin_date + timedelta(days=7)),
            "rating": "9.1Superb54 reviews",
            "price": 123400,
            "name": "walderschof",
            "url": "https://booking.com",
        }
    }

    return availability_lookup[resort_id]
