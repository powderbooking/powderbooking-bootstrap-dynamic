from datetime import date, datetime

from app.utils import get_env_or_default

FAILURE_THRESHOLD = int(get_env_or_default("FAILURE_THRESHOLD", "2"))
BACKEND_API = get_env_or_default("BACKEND_API", "http://0.0.0.0:5000")
# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
# 2023-06-18T16:34:56.470Z
DATETIME_BACKEND_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
DATE_BACKEND_FORMAT = "%Y-%m-%d"


def date_parse(date: str) -> date:
    return datetime.strptime(date, DATE_BACKEND_FORMAT).date()


def datetime_parse(dt: str) -> datetime:
    return datetime.strptime(dt, DATETIME_BACKEND_FORMAT)
