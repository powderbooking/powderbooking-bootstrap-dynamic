from datetime import date, timedelta

from app.api.availabilityApi import getAvailabilities, postAvailability
from app.availability import availabilities
from app.bootstrapper import Bootstrapper
from app.config import date_parse


class AvailabilityBootstrapper(Bootstrapper):
    def _check(self, resort_id: int) -> bool:
        known = getAvailabilities(resort_id)

        if not known:
            return False

        cutoff = date.today() + timedelta(days=2)

        valids = [x for x in known if date_parse(x["checkin"]) >= cutoff]

        return bool(valids)

    def _post(self, resort_id: int) -> bool:
        dto = availabilities(resort_id)
        return postAvailability(dto)
