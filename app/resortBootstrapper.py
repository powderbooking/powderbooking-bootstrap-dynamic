from app.api.resortApi import getResort, postResort
from app.bootstrapper import Bootstrapper
from app.resort import resorts


class ResortBootstrapper(Bootstrapper):
    def _check(self, resort_id: int) -> bool:
        knownResort = getResort(resort_id)
        return knownResort is not None

    def _post(self, resort_id: int) -> bool:
        resort = resorts(resort_id)
        return postResort(resort_id, resort)
