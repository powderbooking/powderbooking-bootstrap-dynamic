from datetime import datetime, timedelta

from app.api.weatherApi import getWeather, postWeather
from app.bootstrapper import Bootstrapper
from app.config import datetime_parse
from app.weather import weathers


class WeatherBootstrapper(Bootstrapper):
    def _check(self, resort_id: int) -> bool:
        known = getWeather(resort_id)

        if not known or not known["dt"]:
            return False

        dt = datetime_parse(known["dt"])
        cutoff = datetime.now() - timedelta(hours=5)

        return dt >= cutoff

    def _post(self, resort_id: int) -> bool:
        resort = weathers(resort_id)
        return postWeather(resort)
