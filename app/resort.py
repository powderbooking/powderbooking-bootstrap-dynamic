#  Copyright (c) 2021. Michael Kemna.


from app.api.resortApi import ResortDto


def resorts(resort_id: int) -> ResortDto:
    resorts_lookup = {
        -1: {
            "id": -1,
            "booking_dest_id": "900040528",
            "continent": "Europe",
            "country": "Austria",
            "village": "Kaprun",
            "lat": 47.1833333,
            "lng": 12.6833333,
            "altitude_min_m": 1051,
            "altitude_max_m": 1978,
            "lifts": 17,
            "slopes_total_km": 41,
            "slopes_blue_km": 13,
            "slopes_red_km": 25,
            "slopes_black_km": 3,
        }
    }

    return resorts_lookup[resort_id]
