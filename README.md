# powderbooking-bootstrap-dynamic

Bootstrap the database with dynamic (test-)data for the Powderbooking application 

## How to use

#### Python

To run:
1. `poetry install --no-root`
2. set `.env` based on `.env.template`
3. the backend must run to be able to test the scraper
4. `poetry run python3 app.py --api [API]`
   - `[API]` is either forecast or weather

To test: `poetry run pytest`

To lint: `poetry run ruff check`

To format: `poetry run ruff format`

You are encouraged to [integrate ruff with your editor](https://docs.astral.sh/ruff/editors/setup/)